# Interactive visualisation of train unit schedules

Creates a visual representation of a train unit schedule with interactive features to explore and evaluate the schedule data.

### Requirments to run the program
1. A Java Development Kit (JDK) version 8.0+, including JavaSwing (Standard with Oracle JDK 8).
2. A JAR file of the gephi-toolkit library version 0.9.2, this can be found at https://gephi.org/toolkit/.
3. Apache Ant.

### Running the program
1. Clone the repository.
2. If the gephi-toolkit jar is not in the lib folder make sure to add it.
3. In the main directory open a terminal and type "ant run" to compile and run the program.

### Other Ant commands
1. "ant compile": Compiles the source files.
2. "ant jar": Creates a jar file of the program (Will need the gephi-toolkit jar to work).
3. "ant clean": Removes any compiled source code and jar.


## Loading Data
The application has been set up to load train schedule data from either:
*  The raw train schedule files.
*  Separate CSV files.

Examples of CSV files and historical train schedule data can be found under sample_files in the repository to show the file format in more detail.

Files can be loaded into the application either through the 'Import Data' section of the control panel or by going to File > Import Data > File Type.

#### Arc Data (Deployments)
##### Raw data
The raw arc data can be found in the lastOptArcs.xml files in the historical schedule dataset, the arc data is set in an \<optArcSol\> tag containing the following arc data fromTrain, toTrain, fromTrainIdx, toTrainIdx, arcIndx, arcKind, Value, Type
##### CSV file
The arc data can be represented in a CSV file, the first line describes the arc attributes which are Id, fromTrain, toTrain, Source, Target, arcKind, Value, Type. Each following line contains the details of a single arc in the same format as the header line.

#### Node Data (Journeys)
##### Raw Data
The raw node data can be found in the Node_Input.dat file in the historical schedule dataset, the node data is defined in an 'originalNODE' array where each entry defines a single node.
##### CSV file
The node data can be represented in a CSV file, the first line describes the node attributes which are Id, trainID, Source, Departure-Time, Destination, Arrival-Time. Each following line contains the details of a single node in the same format as the header line.

#### Path Data
##### Raw Data
The raw path data can be found in the lastOptArcs.xml files in the historical schedule dataset, the path data is defined in a set of \<optPathSol\> tags under the \<optArcSol\> tags, these are loaded in automatically when the arc data is loaded from the lastOptArcs.xml.
##### CSV file
The path data can be represented in a CSV file, the first line describes the path attributes which are PathNodeList, PathArcList, NumberUnits, Cost. Each following line describes a single path in the same format as the header line.

## Features
The features have been organised into separate panels each with their own uses.

#### Control Panel
The control panel hosts most of the features that allow the user to manipulate the data and design to be used in the graph.
##### Design Features
The design of the graph can be altered to represent different elements of the data. The design can be reset to default with the reset button.
*  Background Colour: Changes the background colour of the graph to the selected colour in the combo box.
*  Journey Partition: This partitions the nodes based on either the Source or Destination station name, meaning every node with the same station value will be coloured the same in the graph.
*  Layout Method: Changes the layout of the graph to the applied layout, layouts are algorithms that function on the graph data over time, applying some filters multiple times can produce more organised results.
*  Labelling: Allows labels to be set on the nodes and arcs of the graph, this can be used to represent some of the loaded data in the visualisation.

##### Data Filtering
The data being shown in the graph can be filtered, this removes nodes and arcs with certain attributes out of the view, the filter can be removed using the reset button.
*  By Degree Filter: This filters nodes based on their degree (number of neighbours), the filter can be applied to either nodes with greater than, equal to or less than the value entered by the user.
*  By station partition: If a partition has been applied to the nodes then a station partition can be applied, this will filter out all nodes that do not have the selected station as their partition.

#### Data Panel
The data panel found on the right side of the application provides access to the loaded data in the graph.
*  Clicking a path in the 'Path Details' table will highlight the path in the graph, this can be reset by the design reset button.

### Exporting
The application is able to export the graph in several different formats.
*  Graph file (.gexf): Exporting in this format will allow for the graph data to be loaded back into the application.
*  Png file: This exports the graph view as an image.
*  Pdf file: This exports the graph view as an image in a pdf file.

