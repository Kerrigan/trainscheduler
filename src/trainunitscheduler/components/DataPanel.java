package trainunitscheduler.components;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.EdgeIterable;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.NodeIterable;
import org.openide.util.Exceptions;

/**
 *
 * @author Connor
 */
public class DataPanel extends javax.swing.JPanel{
    
    private DefaultTableModel pathTableModel;
    private ListSelectionModel pathSelectionModel;
    private DefaultTableModel nodeTableModel;
    private DefaultTableModel arcTableModel;
    
    
    /**
     * Creates new form DataPanel
     */
    public DataPanel() {
	initComponents();
	setupTables();
    }
    
    /**
     * This method is called from within the constructor to initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ClickedNodeDetailPanel = new javax.swing.JPanel();
        clickedJourneyDetailsLabel = new javax.swing.JLabel();
        pathDetailsLabel = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        PathTable = new javax.swing.JTable();
        nodeDetailsLabel = new javax.swing.JLabel();
        arcDetailsLabel = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        NodeTable = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        ArcTable = new javax.swing.JTable();
        idClickedNodeLabel = new javax.swing.JLabel();
        labelClickedNodeLabel = new javax.swing.JLabel();
        trainIDClickedNodeLabel = new javax.swing.JLabel();
        sourceClickedNodeLabel = new javax.swing.JLabel();
        departureClickedNodeLabel = new javax.swing.JLabel();
        destinationClickedNodeLabel = new javax.swing.JLabel();
        arrivalClickedNodeLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        DepartureTimeLabel = new javax.swing.JLabel();
        IdLabel = new javax.swing.JLabel();
        LabelLabel = new javax.swing.JLabel();
        TrainIdLabel = new javax.swing.JLabel();
        SourceLabel = new javax.swing.JLabel();
        DestinationLabel = new javax.swing.JLabel();
        ArrivalTimeLabel = new javax.swing.JLabel();
        clickPathLabel = new javax.swing.JLabel();
        nodeDescriptionLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();

        ClickedNodeDetailPanel.setPreferredSize(new java.awt.Dimension(300, 300));

        clickedJourneyDetailsLabel.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(clickedJourneyDetailsLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.clickedJourneyDetailsLabel.text")); // NOI18N

        pathDetailsLabel.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(pathDetailsLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.pathDetailsLabel.text")); // NOI18N

        PathTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Node Path", "Arc Path", "No. of Units", "Cost"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        PathTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane3.setViewportView(PathTable);

        nodeDetailsLabel.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(nodeDetailsLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.nodeDetailsLabel.text")); // NOI18N

        arcDetailsLabel.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(arcDetailsLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.arcDetailsLabel.text")); // NOI18N

        NodeTable.setAutoCreateRowSorter(true);
        NodeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "trainID", "Source", "Departure-Time", "Destination", "Arrival-Time"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        NodeTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane4.setViewportView(NodeTable);

        ArcTable.setAutoCreateRowSorter(true);
        ArcTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "fromTrain", "toTrain", "Arc Kind", "Value"
            }
        ));
        ArcTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane5.setViewportView(ArcTable);

        idClickedNodeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(idClickedNodeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.idClickedNodeLabel.text")); // NOI18N

        labelClickedNodeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(labelClickedNodeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.labelClickedNodeLabel.text")); // NOI18N

        trainIDClickedNodeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(trainIDClickedNodeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.trainIDClickedNodeLabel.text")); // NOI18N

        sourceClickedNodeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(sourceClickedNodeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.sourceClickedNodeLabel.text")); // NOI18N

        departureClickedNodeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(departureClickedNodeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.departureClickedNodeLabel.text")); // NOI18N

        destinationClickedNodeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(destinationClickedNodeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.destinationClickedNodeLabel.text")); // NOI18N

        arrivalClickedNodeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(arrivalClickedNodeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.arrivalClickedNodeLabel.text")); // NOI18N

        DepartureTimeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(DepartureTimeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.DepartureTimeLabel.text")); // NOI18N

        IdLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(IdLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.IdLabel.text")); // NOI18N

        LabelLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(LabelLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.LabelLabel.text")); // NOI18N

        TrainIdLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(TrainIdLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.TrainIdLabel.text")); // NOI18N

        SourceLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(SourceLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.SourceLabel.text")); // NOI18N

        DestinationLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(DestinationLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.DestinationLabel.text")); // NOI18N

        ArrivalTimeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(ArrivalTimeLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.ArrivalTimeLabel.text")); // NOI18N

        clickPathLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(clickPathLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.clickPathLabel.text")); // NOI18N

        nodeDescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(nodeDescriptionLabel, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.nodeDescriptionLabel.text")); // NOI18N

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.jLabel1.text")); // NOI18N

        javax.swing.GroupLayout ClickedNodeDetailPanelLayout = new javax.swing.GroupLayout(ClickedNodeDetailPanel);
        ClickedNodeDetailPanel.setLayout(ClickedNodeDetailPanelLayout);
        ClickedNodeDetailPanelLayout.setHorizontalGroup(
            ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ClickedNodeDetailPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(ClickedNodeDetailPanelLayout.createSequentialGroup()
                        .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ClickedNodeDetailPanelLayout.createSequentialGroup()
                                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(departureClickedNodeLabel)
                                    .addComponent(idClickedNodeLabel)
                                    .addComponent(labelClickedNodeLabel)
                                    .addComponent(trainIDClickedNodeLabel)
                                    .addComponent(sourceClickedNodeLabel)
                                    .addComponent(destinationClickedNodeLabel)
                                    .addComponent(arrivalClickedNodeLabel))
                                .addGap(18, 18, 18)
                                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ArrivalTimeLabel)
                                    .addComponent(DestinationLabel)
                                    .addComponent(SourceLabel)
                                    .addComponent(TrainIdLabel)
                                    .addComponent(LabelLabel)
                                    .addComponent(IdLabel)
                                    .addComponent(DepartureTimeLabel)))
                            .addGroup(ClickedNodeDetailPanelLayout.createSequentialGroup()
                                .addComponent(pathDetailsLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(clickPathLabel))
                            .addGroup(ClickedNodeDetailPanelLayout.createSequentialGroup()
                                .addComponent(nodeDetailsLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nodeDescriptionLabel))
                            .addGroup(ClickedNodeDetailPanelLayout.createSequentialGroup()
                                .addComponent(arcDetailsLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel1))
                            .addComponent(clickedJourneyDetailsLabel))
                        .addGap(0, 79, Short.MAX_VALUE)))
                .addContainerGap())
        );
        ClickedNodeDetailPanelLayout.setVerticalGroup(
            ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ClickedNodeDetailPanelLayout.createSequentialGroup()
                .addComponent(clickedJourneyDetailsLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idClickedNodeLabel)
                    .addComponent(IdLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelClickedNodeLabel)
                    .addComponent(LabelLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trainIDClickedNodeLabel)
                    .addComponent(TrainIdLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sourceClickedNodeLabel)
                    .addComponent(SourceLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(departureClickedNodeLabel)
                    .addComponent(DepartureTimeLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(destinationClickedNodeLabel)
                    .addComponent(DestinationLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(arrivalClickedNodeLabel)
                    .addComponent(ArrivalTimeLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pathDetailsLabel)
                    .addComponent(clickPathLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nodeDetailsLabel)
                    .addComponent(nodeDescriptionLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ClickedNodeDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(arcDetailsLabel)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(DataPanel.class, "DataPanel.jLabel2.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ClickedNodeDetailPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ClickedNodeDetailPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 732, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable ArcTable;
    private javax.swing.JLabel ArrivalTimeLabel;
    private javax.swing.JPanel ClickedNodeDetailPanel;
    private javax.swing.JLabel DepartureTimeLabel;
    private javax.swing.JLabel DestinationLabel;
    private javax.swing.JLabel IdLabel;
    private javax.swing.JLabel LabelLabel;
    private javax.swing.JTable NodeTable;
    private javax.swing.JTable PathTable;
    private javax.swing.JLabel SourceLabel;
    private javax.swing.JLabel TrainIdLabel;
    private javax.swing.JLabel arcDetailsLabel;
    private javax.swing.JLabel arrivalClickedNodeLabel;
    private javax.swing.JLabel clickPathLabel;
    private javax.swing.JLabel clickedJourneyDetailsLabel;
    private javax.swing.JLabel departureClickedNodeLabel;
    private javax.swing.JLabel destinationClickedNodeLabel;
    private javax.swing.JLabel idClickedNodeLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel labelClickedNodeLabel;
    private javax.swing.JLabel nodeDescriptionLabel;
    private javax.swing.JLabel nodeDetailsLabel;
    private javax.swing.JLabel pathDetailsLabel;
    private javax.swing.JLabel sourceClickedNodeLabel;
    private javax.swing.JLabel trainIDClickedNodeLabel;
    // End of variables declaration//GEN-END:variables
    
    // Updates labels of clicked node details
    void updateClickedNodeData() {
	FileReader fileReader = null;
	try {
	    File file = new File("clickednodeFile.csv");
	    fileReader = new FileReader(file);
	    try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
		String line;
		String[] nodeDetails = new String[7];
		int x = 0;
		while((line = bufferedReader.readLine()) != null)
		{
		    String values[] = line.split(",");
		    nodeDetails[x] = values[1];
		    x++;
		}
		
		// Set labels to node details
		IdLabel.setText(nodeDetails[0]);
		LabelLabel.setText(nodeDetails[1]);
		TrainIdLabel.setText(nodeDetails[2]);
		SourceLabel.setText(nodeDetails[3]);
		DepartureTimeLabel.setText(nodeDetails[4]);
		DestinationLabel.setText(nodeDetails[5]);
		ArrivalTimeLabel.setText(nodeDetails[6]);
	    }
	    fileReader.close();
	    file.delete();
	    
	} catch (FileNotFoundException ex) {
	} catch (IOException ex) {
	    Exceptions.printStackTrace(ex);
	}
    }
    
    // Updates path table to new paths
    void updatePathTable(File file)
    {
	FileReader fileReader = null;
	try {
	    fileReader = new FileReader(file);
	    BufferedReader bufferedReader = new BufferedReader(fileReader);
	    String line = bufferedReader.readLine(); // Skips header line
	    int pathCount = 0;
	    while((line = bufferedReader.readLine()) != null)
	    {
		String[] values = line.split(",");
		String[] rowdata = new String[5];
		
		rowdata[0] = String.valueOf(pathCount);
		rowdata[1] = values[0].replace(";", ",");
		rowdata[2] = values[1].replace(";", ",");
		rowdata[3] = values[2];
		rowdata[4] = values[3];
		
		pathTableModel.addRow(rowdata);
		pathCount++;
	    }
	    
	} catch (FileNotFoundException ex) {
	    Exceptions.printStackTrace(ex);
	} catch (IOException ex) {
	    Exceptions.printStackTrace(ex);
	}
    }
    
    // Resets path table
    void resetPathTable()
    {
	while(pathTableModel.getRowCount() > 0)
	{
	    pathTableModel.removeRow(0);
	}
    }
    
    // Resets node table
    void resetNodeTable()
    {
	while(nodeTableModel.getRowCount() > 0)
	{
	    nodeTableModel.removeRow(0);
	}
    }
    
    // Resets arc table
    void resetArcTable()
    {
	while(arcTableModel.getRowCount() > 0)
	{
	    arcTableModel.removeRow(0);
	}
    }
    
    // Sets up the tables
    private void setupTables() {
	pathTableModel = (DefaultTableModel) PathTable.getModel();
	pathSelectionModel = PathTable.getSelectionModel();
	
	nodeTableModel = (DefaultTableModel) NodeTable.getModel();
	
	arcTableModel = (DefaultTableModel) ArcTable.getModel();
    }
    
    // Returns the selected path entry
    public String[] getSelectedPathRow()
    {
	int row = PathTable.getSelectedRow();
	String[] rowData = new String[pathTableModel.getColumnCount()];
	int x = 0;
	while(x < pathTableModel.getColumnCount())
	{
	    rowData[x] = pathTableModel.getValueAt(row, x).toString();
	    x++;
	}
	return rowData;
    }
    
    // Listens for which path is selected
    public void addPathSelectionListener(ListSelectionListener sl)
    {
	pathSelectionModel.addListSelectionListener(sl);
    }
    
    // Adds the node data to the table
    public void updateNodeTable(Graph graph)
    {
	NodeIterable nodes = graph.getNodes();
	
	for(Node node: nodes)
	{
	    Object[] nodeData = new String[6];
	    nodeData[0] = node.getAttribute("Id").toString();
	    nodeData[1] = node.getAttribute("trainID").toString();
	    nodeData[2] = node.getAttribute("Source").toString();
	    nodeData[3] = node.getAttribute("Departure-Time").toString();
	    nodeData[4] = node.getAttribute("Destination").toString();
	    nodeData[5] = node.getAttribute("Arrival-Time").toString();
	    
	    nodeTableModel.addRow(nodeData);
	}
    }
    
    // Adds the arc data to the table
    public void updateArcTable(Graph graph)
    {
	EdgeIterable edges = graph.getEdges();
	
	for(Edge edge: edges)
	{
	    Object[] edgeData = new String[5];
	    edgeData[0] = edge.getAttribute("Id").toString();
	    edgeData[1] = edge.getAttribute("fromTrain").toString();
	    edgeData[2] = edge.getAttribute("toTrain").toString();
	    edgeData[3] = edge.getAttribute("arcKind").toString();
	    edgeData[4] = edge.getAttribute("Value").toString();
	    
	    arcTableModel.addRow(edgeData);
	}
    }
}
