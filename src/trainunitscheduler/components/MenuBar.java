package trainunitscheduler.components;

import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Connor
 */
public class MenuBar extends JMenuBar {
    JMenu fileMenu, viewMenu, exportMenu, importMenu;
    JMenuItem exitItem, graphItem, pdfItem, pngItem, importPathItem, importNodeItem, importArcItem;
    JMenuItem centerViewItem;
    JMenuItem helpItem;
    
    public MenuBar()
    {
	// Initialise items for File Menu
	fileMenu = new JMenu("File");
	
	importMenu = new JMenu("Import Data");
	importPathItem = new JMenuItem("Path Data");
	importArcItem = new JMenuItem("Arc Data");
	importNodeItem = new JMenuItem("Node Data");
	importNodeItem.setEnabled(false);
	
	exportMenu = new JMenu("Export As");
	graphItem = new JMenuItem("Graph");
	pdfItem = new JMenuItem("Pdf");
	pngItem = new JMenuItem("Png");
	
	importMenu.add(importArcItem);
	importMenu.add(importNodeItem);
	importMenu.add(importPathItem);
	fileMenu.add(importMenu);
	
	fileMenu.addSeparator();
	
	exportMenu.add(graphItem);
	exportMenu.add(pdfItem);
	exportMenu.add(pngItem);
	fileMenu.add(exportMenu);
	
	fileMenu.addSeparator();
	
	exitItem = new JMenuItem("Exit");
	fileMenu.add(exitItem);
	
	// Initialise items for View Menu
	viewMenu = new JMenu("View");
	
	centerViewItem = new JMenuItem("Center View");
	viewMenu.add(centerViewItem);
	
	helpItem = new JMenuItem("Help");
	
	// Add Menus to bar
	this.add(fileMenu);
	this.add(viewMenu);
	this.add(helpItem);
    }
    
    // add Listener methods
    public void addExitActionListener(ActionListener al)
    {
	exitItem.addActionListener(al);
    }
    
    public void addCenterActionListener(ActionListener al)
    {
	centerViewItem.addActionListener(al);
    }
    
    public void addHelpActionListener(ActionListener al)
    {
	helpItem.addActionListener(al);
    }
    
    public void addExportGraphActionListener(ActionListener al)
    {
	graphItem.addActionListener(al);
    }
    
    public void addExportPdfActionListener(ActionListener al)
    {
	pdfItem.addActionListener(al);
    }
    
    public void addExportPngActionListener(ActionListener al)
    {
	pngItem.addActionListener(al);
    }
    
    public void addImportArcActionListener(ActionListener al)
    {
	importArcItem.addActionListener(al);
    }
    
    public void addImportNodeActionListener(ActionListener al)
    {
	importNodeItem.addActionListener(al);
    }
    
    public void addImportPathActionListener(ActionListener al)
    {
	importPathItem.addActionListener(al);
    }
    
    // Method to enable/disable node import item
    public void nodeMenuControl(boolean enabled)
    {
	importNodeItem.setEnabled(enabled);
    }
}
