package trainunitscheduler.components;

import trainunitscheduler.functions.LoadedData;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import org.gephi.appearance.api.AppearanceController;
import org.gephi.appearance.api.AppearanceModel;
import org.gephi.appearance.api.Function;
import org.gephi.appearance.api.Partition;
import org.gephi.appearance.api.PartitionFunction;
import org.gephi.appearance.plugin.PartitionElementColorTransformer;
import org.gephi.appearance.plugin.palette.Palette;
import org.gephi.appearance.plugin.palette.PaletteManager;
import org.gephi.filters.api.FilterController;
import org.gephi.filters.api.Query;
import org.gephi.filters.api.Range;
import org.gephi.filters.plugin.graph.DegreeRangeBuilder.DegreeRangeFilter;
import org.gephi.filters.plugin.partition.PartitionBuilder.NodePartitionFilter;
import org.gephi.graph.api.Column;
import org.gephi.graph.api.ColumnIterable;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.GraphView;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.NodeIterable;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.EdgeDirectionDefault;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.preview.api.G2DTarget;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.api.RenderTarget;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import static trainunitscheduler.functions.FileParser.parseArc;
import static trainunitscheduler.functions.FileParser.parseNode;
import static trainunitscheduler.functions.ApplyLayout.applyLayout;
/**
 *
 * @author Connor
 */
public class MainFrame extends JFrame
{
    private ImportController importController;
    private Workspace workspace;
    private Container container;
    private PreviewController previewController;
    private ProjectController projectController;
    private G2DTarget target;
    private PreviewModel previewModel;
    private LogPanel logPanel;
    private ControlPanel controlPanel;
    private LoadedData loadedData;
    private PreviewPanel previewPanel;
    private MenuBar menuBar;
    private ExportController exportController;
    private AppearanceController appearanceController;
    private DataPanel dataPanel;
    private GraphView curView;
    private boolean partitionVal = false;
    private String curPartition = null;
    
    // Main script, ran to initialise the application
    public void script() {
	
	// Sets the ui to look like current system os ui
	// If linux os use default look (incompatability with layout used)
	try {
	    String OS = System.getProperty("os.name").toLowerCase();
	    if(OS.contains("mac") || OS.contains("win"))
	    {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }
	} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
	    Exceptions.printStackTrace(ex);
	}
	
	// Initialise a project - and therefore a workspace
	projectController = Lookup.getDefault().lookup(ProjectController.class);
	projectController.newProject();
	workspace = projectController.getCurrentWorkspace();
	importController = Lookup.getDefault().lookup(ImportController.class);
	exportController = Lookup.getDefault().lookup(ExportController.class);
	
	// Sets up the project, loads intialiser data
	setupContainer();
	
	// Append imported file data to GraphAPI
	importController.process(container, new DefaultProcessor(), workspace);
	
	// Configure Preview
	previewSetup();
	
	// New Processing target to render graph
	target = (G2DTarget) previewController.getRenderTarget(RenderTarget.G2D_TARGET);
	
	// Setup BorderLayout for application
	this.setLayout(new BorderLayout());
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	// Initialise Components
	dataPanel = new DataPanel();
	previewPanel = new PreviewPanel(target, dataPanel);
	logPanel = new LogPanel();
	controlPanel = new ControlPanel();
	menuBar = new MenuBar();
	
	// Add action listeneres for communication between components (set up functions)
	setupListeners();
	
	// Add Components to sheduler frame
	this.add(previewPanel, BorderLayout.CENTER);
	this.add(controlPanel, BorderLayout.WEST);
	this.add(logPanel, BorderLayout.SOUTH);
	this.add(menuBar, BorderLayout.NORTH);
	this.add(dataPanel, BorderLayout.EAST);
	
	// Set size and make frame visable
	this.setSize(1600, 1000);
	this.setVisible(true);
	
	// Refresh graph preview and prompt user to load data in log
	previewController.refreshPreview();
	logPanel.log("Open schedule arc and node data or open an existing graph to get started");
	
	// Get the appearanceController
	appearanceController = Lookup.getDefault().lookup(AppearanceController.class);
    }
    
    // Append new columns to control panel node label combo box
    public void setNodeLabelColumns()
    {
	GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
	ColumnIterable columnIterable = graphModel.getGraph().getNodes().toArray()[0].getAttributeColumns();
	String[] columns = new String[columnIterable.toArray().length];
	int x = 0;
	for(Column col : columnIterable)
	{
	    columns[x] = col.getTitle();
	    x++;
	}
	controlPanel.setNodeColumns(columns);
    }
    
    // Append new columns to control panel arc label combo box
    public void setArcLabelColumns()
    {
	try
	{
	    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
	    ColumnIterable columnIterable = graphModel.getGraph().getEdges().toArray()[0].getAttributeColumns();
	    String[] columns = new String[columnIterable.toArray().length];
	    int x = 0;
	    for(Column col : columnIterable)
	    {
		columns[x] = col.getTitle();
		x++;
	    }
	    controlPanel.setArcColumns(columns);
	}
	catch(ArrayIndexOutOfBoundsException e)
	{
	    // arc columns not found
	}
    }
    
    // Load graph file
    public void loadGraphFile(File graphFile)
    {
	try {
	    // If file not a graph file throw exception to be caught
	    if(!graphFile.getName().contains(".gexf"))
	    {
		throw new IllegalArgumentException();
	    }
	    container = importController.importFile(graphFile);
	    
	    projectController.newProject();
	    workspace = projectController.getCurrentWorkspace();
	    importController.process(container, new DefaultProcessor(), workspace);
	    
	    // Refresh preview
	    previewController.refreshPreview();
	    
	    logPanel.log("Graph loaded from " + graphFile.getName());
	    loadedData.setFileloaded(true);
	    previewPanel.setarcFile(graphFile.getName() + " Arcs");
	    previewPanel.setnodeFile(graphFile.getName() + " Nodes");
	    
	    // Append new columns to label combo boxes
	    setNodeLabelColumns();
	    setArcLabelColumns();
	    
	    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
	    
	    // Append stations to station filter combo box
	    controlPanel.setNodeStations(graphModel.getGraph());
	    
	    // Reset path table and selection
	    dataPanel.resetPathTable();
	    previewPanel.setnodeSel("");
	    previewPanel.setarcSel("");
	    
	    dataPanel.updateNodeTable(graphModel.getGraph());
	    dataPanel.updateArcTable(graphModel.getGraph());
	    
	} catch (IllegalArgumentException | FileNotFoundException | NullPointerException ex ) {
	    logPanel.log("Error loading graph from file " + graphFile.getName());
	}
    }
    
    // Initialises the workspace and preview with default values
    void previewSetup()
    {
	previewController = Lookup.getDefault().lookup(PreviewController.class);
	previewModel = previewController.getModel();
	previewModel.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
	previewModel.getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(Color.BLACK));
	previewModel.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
	previewModel.getProperties().putValue(PreviewProperty.EDGE_OPACITY, 50);
	previewModel.getProperties().putValue(PreviewProperty.EDGE_RADIUS, 10f);
	previewModel.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, Color.WHITE);
	loadedData = new LoadedData();
    }
    
    // Initial setup, sets up the container with an initialising graph
    private void setupContainer() {
	// Retrieve data from internal initialiser graph file
	try {
	    InputStream fileinput;
	    fileinput = this.getClass().getResourceAsStream("/trainunitscheduler/resources/Initialiser.gexf");
	    BufferedReader bufferedReader;
	    bufferedReader = new BufferedReader(new InputStreamReader(fileinput));
	    String line;
	    File newfile = new File("newfile.gexf");
	    FileWriter fileWriter;
	    fileWriter = new FileWriter("newfile.gexf");
	    BufferedWriter bufferedWriter;
	    bufferedWriter = new BufferedWriter(fileWriter);
	    while((line = bufferedReader.readLine()) != null) {
		bufferedWriter.write(line + "\n");
	    }
	    bufferedWriter.close();
	    fileWriter.close();
	    bufferedReader.close();
	    fileinput.close();
	    
	    container = importController.importFile(newfile);
	    newfile.delete();
	    
	} catch (IllegalArgumentException | FileNotFoundException ex) {
	    
	} catch (IOException ex) {
	    Exceptions.printStackTrace(ex);
	}
    }
    
    // Method that handles graph import
    public void opengraphButtonPressed()
    {
	JFileChooser fileChooser;
	fileChooser = new JFileChooser();
	int returnVal = fileChooser.showOpenDialog(MainFrame.this);
	
	if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	    logPanel.log("Opening " + fileChooser.getSelectedFile().getName());
	    loadGraphFile(fileChooser.getSelectedFile());
	}
	else
	{
	    logPanel.log("User cancelled opening of an existing graph");
	}
	controlPanel.nodeButtonControl(false);
	menuBar.nodeMenuControl(false);
    }
    
    // Method that handles the importing of arc data
    public void arcImport()
    {
	// Open file chooser and get user to select arc data file
	JFileChooser fileChooser;
	fileChooser = new JFileChooser();
	int returnVal = fileChooser.showOpenDialog(this);
	
	if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	    controlPanel.nodeButtonControl(true);
	    menuBar.nodeMenuControl(true);
	    int choice;
	    if(loadedData.isFileloaded())
	    {
		// If file loaded then ask user if they want to append the data or start a new graph
		choice = JOptionPane.showConfirmDialog(this, "There is already a file loaded, do you want to append the arc data to the graph?", "Existing Graph", JOptionPane.YES_NO_OPTION);
		
		if(choice == JOptionPane.OK_OPTION)
		{
		    // Append the arc data to the current graph
		    try {
			// Graph read unlock
			Lookup.getDefault().lookup(GraphController.class).getGraphModel().getGraph().readUnlockAll();
			
			File arcfile = parseArc(fileChooser.getSelectedFile());
			
			// Make sure arc data was successfully parsed
			if(arcfile == null)
			{
			    logPanel.log("Invalid arc data!");
			    return;
			}
			
			container = importController.importFile(arcfile);
			container.getLoader().setEdgeDefault(EdgeDirectionDefault.DIRECTED);   //Force DIRECTED
			
			importController.process(container, new DefaultProcessor(), workspace);
			
			previewController.refreshPreview();
			
			logPanel.log("Arc data appended from " + fileChooser.getSelectedFile().getName());
			
		    } catch (IOException ex) {
			logPanel.log("Issue opening file!");
		    }
		}
		else{
		    // Start a new graph and add the arc data
		    choice = JOptionPane.showConfirmDialog(this, "Do you want to start a new graph instead?", "New Graph", JOptionPane.YES_NO_OPTION);
		    
		    if(choice == JOptionPane.OK_OPTION)
		    {
			try {
			    File arcfile = parseArc(fileChooser.getSelectedFile());
			    
			    // Make sure arc data was parsed correctly
			    if(arcfile == null)
			    {
				logPanel.log("File " + fileChooser.getSelectedFile().getName() + " invalid arc data!");
				return;
			    }
			    
			    // Create new graph and append it to workspace
			    loadedData.setFileloaded(false);
			    loadedData.setNodesloaded(false);
			    
			    projectController.newProject();
			    workspace = projectController.getCurrentWorkspace();
			    container = importController.importFile(arcfile);
			    container.getLoader().setEdgeDefault(EdgeDirectionDefault.DIRECTED);
			    importController.process(container, new DefaultProcessor(), workspace);
			    
			    previewController.refreshPreview();
			    
			    logPanel.log("Arc data loaded from " + fileChooser.getSelectedFile().getName());
			    loadedData.setArcsloaded(true);
			    
			} catch (FileNotFoundException ex) {
			    logPanel.log("Issue opening file!");
			}
		    }
		    else
		    {
			logPanel.log("User cancelled loading of " + fileChooser.getSelectedFile().getName());
		    }
		}
	    }
	    else
	    {
		// If no file loaded then load arc data
		try {
		    File arcfile = parseArc(fileChooser.getSelectedFile());
		    projectController.newProject();
		    workspace = projectController.getCurrentWorkspace();
		    container = importController.importFile(arcfile);
		    container.getLoader().setEdgeDefault(EdgeDirectionDefault.DIRECTED);
		    importController.process(container, new DefaultProcessor(), workspace);
		    previewController.refreshPreview();
		    loadedData.setFileloaded(true);
		} catch (FileNotFoundException ex) {
		    logPanel.log("Issue opening file!");
		}
	    }
	    logPanel.log("Opening arc data from " + fileChooser.getSelectedFile().getName());
	    
	    // Append new arc columns to label combo box
	    setArcLabelColumns();
	    
	    Graph graph = Lookup.getDefault().lookup(GraphController.class).getGraphModel().getGraph();
	    
	    // Refreshes the data tables
	    dataPanel.resetNodeTable();
	    dataPanel.resetArcTable();
	    // If the file is an arc.xml file consisting of arc and path details import extracted path data
	    if(fileChooser.getSelectedFile().getName().contains(".xml"))
	    {
		dataPanel.resetPathTable();
		dataPanel.updatePathTable(new File("path.csv"));
	    }
	    dataPanel.updateArcTable(graph);
	    
	    loadedData.setArcsloaded(true);
	    previewPanel.setarcFile(fileChooser.getSelectedFile().getName());
	}
	else
	{
	    // User closed filechooser without picking file
	    logPanel.log("User canceled importing arc data");
	    return;
	}
    }
    
    // Method that handles Node Data import
    public void nodeImport()
    {
	JFileChooser fileChooser;
	fileChooser = new JFileChooser();
	int returnVal = fileChooser.showOpenDialog(this);
	
	if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	    ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
	    workspace = pc.getCurrentWorkspace();
	    importController = Lookup.getDefault().lookup(ImportController.class);
	    
	    File nodeFile = parseNode(fileChooser.getSelectedFile());
	    
	    // Make sure node data was successfully parsed
	    if(nodeFile == null)
	    {
		logPanel.log("File " + fileChooser.getSelectedFile().getName() + " invalid node data!");
		return;
	    }
	    
	    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace);
	    DirectedGraph directedGraph = graphModel.getDirectedGraph();
	    
	    // If graph has readlock unlock to append data
	    directedGraph.readUnlockAll();
	    
	    try{
		String lines[] = Files.readAllLines(nodeFile.toPath()).toArray(new String[1]);
		String headers[] = lines[0].split(",");
		
		// Append the new columns
		for(String header : headers)
		{
		    // Add any new columns to the node table
		    if(!graphModel.getNodeTable().hasColumn(header))
		    {
			graphModel.getNodeTable().addColumn(header, String.class);
		    }
		    else
		    {
			if(graphModel.getNodeTable().getColumn(header).getTypeClass() != String.class)
			{
			    graphModel.getNodeTable().removeColumn(header);
			    graphModel.getNodeTable().addColumn(header, String.class);
			}
		    }
		}
		
		int nodeCounter = 0;
		for(String nodeLine : lines)
		{
		    // Skip headers
		    if(nodeCounter == 0)
		    {
			nodeCounter++;
			continue;
		    }
		    
		    // Get or make node by id
		    String values[] = nodeLine.split(",");
		    Node n;
		    if(directedGraph.hasNode(values[0]))
		    {
			n = directedGraph.getNode(values[0]);
		    }
		    else
		    {
			n = graphModel.factory().newNode(values[0]);
		    }
		    
		    // Set attribute values for node
		    int x = 1;
		    while(x < values.length)
		    {
			n.setAttribute(headers[x], values[x]);
			x++;
		    }
		    // Add new or updated node to graph
		    directedGraph.addNode(n);
		    nodeCounter++;
		}
		logPanel.log("Opening node data from " + fileChooser.getSelectedFile().getName());
		previewPanel.setnodeFile(fileChooser.getSelectedFile().getName());
		loadedData.setNodesloaded(true);
		
		// Set default preview properties
		previewController.getModel().getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
		previewController.getModel().getProperties().putValue(PreviewProperty.SHOW_EDGE_LABELS, Boolean.TRUE);
		previewController.getModel().getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, new DependantOriginalColor(Color.BLACK));
		previewController.getModel().getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
		previewController.getModel().getProperties().putValue(PreviewProperty.EDGE_OPACITY, 100);
		previewController.getModel().getProperties().putValue(PreviewProperty.NODE_OPACITY, 80);
		previewController.getModel().getProperties().putValue(PreviewProperty.EDGE_RADIUS, 10f);
		previewController.getModel().getProperties().putValue(PreviewProperty.ARROW_SIZE, 10f);
		
		// Refresh preview
		previewController.refreshPreview();
		
		// Refresh node table
		dataPanel.resetNodeTable();
		dataPanel.updateNodeTable(graphModel.getGraph());
		
		// Append stations to station filter combo box
		controlPanel.setNodeStations(graphModel.getGraph());
		
		// Set the new node columns
		setNodeLabelColumns();
	    }
	    catch(IOException e)
	    {
		logPanel.log("Issue appending node data!");
	    }
	}
	else
	{
	    logPanel.log("User canceled importing node data");
	}
    }
    
    // Setup the project listeners - Most of the applications functionality comes through these listeners
    private void setupListeners() {
	
	// Ensures preview has been drawn before rendering
	this.addComponentListener(new ComponentAdapter() {
	    @Override
	    public void componentShown(ComponentEvent e) {
		previewPanel.resetZoom();
	    }
	});
	
	// Setup control panel listeners
	// Load graph
	controlPanel.addOpenActionListener((ActionEvent e) -> {
	    opengraphButtonPressed();
	});
	
	// Update background colour
	controlPanel.addBackComboListener((ActionEvent e) -> {
	    ControlPanel.ColourPicker colourPicker = controlPanel.getBackColour();
	    previewModel.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, colourPicker.getColour());
	    previewController.refreshPreview();
	    logPanel.log("Background colour changed to " + colourPicker.getColourName());
	});
	
	// Import arc data
	controlPanel.addArcActionListener((ActionEvent e) -> {
	    arcImport();
	});
	
	// Import node data
	controlPanel.addNodeActionListener((ActionEvent e) -> {
	    nodeImport();
	});
	
	// Apply Layout to graph
	controlPanel.addApplyLayoutListener((ActionEvent e) -> {
	    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
	    String layout = controlPanel.getSelLayout();
	    applyLayout(layout, previewController, graphModel);
	    logPanel.log(layout + " Layout applied");
	    previewController.refreshPreview();
	});
	
	// Apply partition to nodes
	controlPanel.addApplyNodePartitionListener((ActionEvent e) ->{
	    
	    AppearanceModel appearanceModel = appearanceController.getModel();
	    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
	    Graph graph = graphModel.getGraph();
	    
	    try{
		// Set partition column to user selected value
		Column column;
		if(controlPanel.getSelNodePartition().equals("Source"))
		{
		    column = graphModel.getNodeTable().getColumn("Source");
		    curPartition = "source";
		}
		else if(controlPanel.getSelNodePartition().equals("Destination"))
		{
		    column = graphModel.getNodeTable().getColumn("Destination");
		    curPartition = "destination";
		    
		}
		else
		{
		    column = null;
		}
		Function func = appearanceModel.getNodeFunction(graph, column, PartitionElementColorTransformer.class);
		Partition partition = ((PartitionFunction) func).getPartition();
		Palette palette = PaletteManager.getInstance().generatePalette(partition.size());
		partition.setColors(palette.getColors());
		appearanceController.transform(func);
		logPanel.log("Partition applied to " + controlPanel.getSelNodePartition() + " of journeys");
		partitionVal = true;
		previewController.refreshPreview();
	    }
	    catch(NullPointerException exception)
	    {
		logPanel.log("Issue partitioning data, ensure journey data has been imported!");
	    }
	});
	
	// Sets labels to visible or not
	controlPanel.addShowLabelListener((ActionEvent e) ->{
	    
	    boolean visible = controlPanel.getShowLabel();
	    logPanel.log("User set show labels to " + visible);
	    
	    previewController.getModel().getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, visible);
	    previewController.getModel().getProperties().putValue(PreviewProperty.SHOW_EDGE_LABELS, visible);
	    
	    // Refresh preview
	    previewController.refreshPreview();
	});
	
	// Set arc labels to user selected value
	controlPanel.addArcLabelListener((ActionEvent e) -> {
	    
	    // If event user has not selected, then skip
	    if(e.getModifiers() == 0)
	    {
		return;
	    }
	    
	    String columnName = controlPanel.getArcLabelColumn();
	    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
	    
	    // Set the labels on the graph edges
	    if(columnName.equals("None"))
	    {
		for(Edge edge: graphModel.getGraph().getEdges())
		{
		    edge.setLabel("");
		}
		logPanel.log("User set arc labels to none");
	    }
	    else
	    {
		for(Edge edge: graphModel.getGraph().getEdges())
		{
		    edge.setLabel(edge.getAttribute(columnName).toString());
		}
		logPanel.log("User set arc labels to " + columnName);
	    }
	    // Refresh preview
	    previewController.refreshPreview();
	});
	
	// Set node labels to user selected value
	controlPanel.addNodeLabelListener((ActionEvent e) -> {
	    
	    // If event user has not selected, then skip
	    if(e.getModifiers() == 0)
	    {
		return;
	    }
	    String columnName = controlPanel.getNodeLabelColumn();
	    GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
	    
	    // Set the node labels in the graph
	    if(columnName.equals("None"))
	    {
		for(Node node: graphModel.getGraph().getNodes())
		{
		    node.setLabel("");
		}
		logPanel.log("User set node labels to none");
	    }
	    else
	    {
		for(Node node: graphModel.getGraph().getNodes())
		{
		    node.setLabel(node.getAttribute(columnName).toString());
		}
		logPanel.log("User set node labels to " + columnName);
	    }
	    
	    previewController.refreshPreview();
	});
	
	// Set label colour to selected colour
	controlPanel.addLabelColourListener((ActionEvent e) ->{
	    ControlPanel.ColourPicker colourPicker = controlPanel.getLabelColour();
	    DependantOriginalColor depColour = new DependantOriginalColor(colourPicker.getColour());
	    previewController.getModel().getProperties().putValue(PreviewProperty.NODE_LABEL_COLOR, depColour);
	    previewController.getModel().getProperties().putValue(PreviewProperty.EDGE_LABEL_COLOR, depColour);
	    previewController.refreshPreview();
	    logPanel.log("Label colour changed to " + colourPicker.getColourName());
	});
	
	// Reset the preview to default values
	controlPanel.addDesignResetButtonListener((ActionEvent e) ->{
	    GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
	    
	    // Reset preview
	    previewController.getModel().getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, Color.WHITE);
	    previewController.getModel().getProperties().putValue(PreviewProperty.EDGE_CURVED, false);
	    previewPanel.resetZoom();
	    
	    // Deselect nodes and arcs
	    previewPanel.setarcSel("");
	    previewPanel.setnodeSel("");
	    
	    // Reset nodes/selection
	    NodeIterable nodes = graphController.getGraphModel().getGraph().getNodes();
	    
	    for(Node node: nodes)
	    {
		graphController.getGraphModel().getGraph().getNode(node.getId()).setColor(Color.BLACK);
	    }
	    previewPanel.setnodeSel("");
	    previewPanel.setarcSel("");
	    
	    previewController.refreshPreview();
	    
	    logPanel.log("Design Reset");
	});
	
	// Apply the by degree filter
	controlPanel.addApplyByDegreeFilterListener((ActionEvent e) ->{
	    
	    FilterController filterController = Lookup.getDefault().lookup(FilterController.class);
	    GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
	    
	    // Graph read unlock
	    graphController.getGraphModel().getGraph().readUnlockAll();
	    
	    String[] filterDetails = controlPanel.getDegreeFilter();
	    int filterDegree = Integer.parseInt(filterDetails[1]);
	    
	    DegreeRangeFilter degreeFilter = new DegreeRangeFilter();
	    degreeFilter.init(graphController.getGraphModel().getGraph());
	    
	    if(filterDetails[0].equals("<"))
	    {
		// Only show nodes with degree less than user defined value
		degreeFilter.setRange(new Range(0, filterDegree));
	    }
	    else if(filterDetails[0].equals("="))
	    {
		// Only show nodes equal to user defined value
		degreeFilter.setRange(new Range(filterDegree, filterDegree));
	    }
	    else if(filterDetails[0].equals(">"))
	    {
		// Only show nodes with degree greater than user defined value
		degreeFilter.setRange(new Range(filterDegree, Integer.MAX_VALUE));
	    }
	    else
	    {
		// Otherwise cancel
		return;
	    }
	    
	    // Apply the filter to the graphview
	    Query query = filterController.createQuery(degreeFilter);
	    curView = filterController.filter(query);
	    graphController.getGraphModel().setVisibleView(curView);
	    previewController.refreshPreview();
	    
	    logPanel.log("Filter on nodes with degree " + filterDetails[0] + " " + filterDegree);
	});
	
	// Reset the filtered out data
	controlPanel.addFilterResetButtonListener((ActionEvent e) ->{
	    
	    // Make sure the current view is filtered
	    if(curView == null)
	    {
		return;
	    }
	    
	    // Ensure graph is unlocked for reading
	    Lookup.getDefault().lookup(GraphController.class).getGraphModel().getGraph().readUnlockAll();
	    
	    GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
	    try{
		graphController.getGraphModel().destroyView(curView);
		logPanel.log("Filtered Data Reset");
	    }
	    catch(IllegalArgumentException exception)
	    {
	    }
	    previewController.refreshPreview();
	    
	});
	
	// Apply the by station filter
	controlPanel.addApplyByStationFilterListener((ActionEvent e) ->{
	    
	    if(partitionVal)
	    {
		
		FilterController filterController = Lookup.getDefault().lookup(FilterController.class);
		GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
		AppearanceModel appearanceModel = Lookup.getDefault().lookup(AppearanceController.class).getModel();
		
		// Graph read unlock
		graphController.getGraphModel().getGraph().readUnlockAll();
		
		String station = controlPanel.getStationFilter();
		Query query = null;
		if(curPartition.equals("source"))
		{
		    // Filter nodes that dont have selected station as a source
		    Column sourceColumn = graphController.getGraphModel().getNodeTable().getColumn("source");
		    NodePartitionFilter sourcePartitionFilter = new NodePartitionFilter(sourceColumn, appearanceModel);
		    sourcePartitionFilter.unselectAll();
		    sourcePartitionFilter.addPart(station);
		    query = filterController.createQuery(sourcePartitionFilter);
		}
		else if(curPartition.equals("destination"))
		{
		    // Filter nodes that dont have selected station as a destination
		    Column sourceColumn = graphController.getGraphModel().getNodeTable().getColumn("destination");
		    NodePartitionFilter destinationPartitionFilter = new NodePartitionFilter(sourceColumn, appearanceModel);
		    destinationPartitionFilter.unselectAll();
		    destinationPartitionFilter.addPart(station);
		    query = filterController.createQuery(destinationPartitionFilter);
		}
		
		curView = filterController.filter(query);
		graphController.getGraphModel().setVisibleView(curView);
		
		previewController.refreshPreview();
		logPanel.log("Filter on station " + station + " applied using partition on " + curPartition);
	    }
	    else
	    {
		logPanel.log("No partition applied to graph!");
	    }
	});
	
	
	// Setup MenuBar listeners
	// Exit program
	menuBar.addExitActionListener((ActionEvent e) -> {
	    System.exit(1);
	});
	
	// Center the view on graph
	menuBar.addCenterActionListener((ActionEvent e) -> {
	    previewPanel.resetZoom();
	    logPanel.log("User cenetered view");
	});
	
	// Display help frame
	menuBar.addHelpActionListener((ActionEvent e) -> {
	    HelpFrame helpFrame = new HelpFrame();
	});
	
	
	// Setup exports
	// Export as gexf file
	menuBar.addExportGraphActionListener((ActionEvent e) -> {
	    
	    JFileChooser chooser = new JFileChooser();
	    int result = chooser.showSaveDialog(null);
	    
	    if (result == JFileChooser.APPROVE_OPTION) {
		try {
		    exportController.exportFile(new File(chooser.getSelectedFile() + ".gexf"));
		    logPanel.log("Graph exported as gexf file '" + chooser.getSelectedFile().getName() + ".gexf'" );
		} catch (IOException ex) {
		    Exceptions.printStackTrace(ex);
		}
	    }
	    else
	    {
		logPanel.log("User cancelled export");
	    }
	});
	
	// Export as pdf
	menuBar.addExportPdfActionListener((ActionEvent e) -> {
	    
	    JFileChooser chooser = new JFileChooser();
	    int result = chooser.showSaveDialog(null);
	    
	    if (result == JFileChooser.APPROVE_OPTION) {
		try {
		    exportController.exportFile(new File(chooser.getSelectedFile() + ".pdf"));
		    logPanel.log("Graph exported as pdf file '" + chooser.getSelectedFile().getName() + ".pdf'" );
		} catch (IOException ex) {
		    Exceptions.printStackTrace(ex);
		}
	    }
	    else
	    {
		logPanel.log("User cancelled export");
	    }
	});
	
	// Export as Png
	menuBar.addExportPngActionListener((ActionEvent e) -> {
	    
	    JFileChooser chooser = new JFileChooser();
	    int result = chooser.showSaveDialog(null);
	    
	    if (result == JFileChooser.APPROVE_OPTION) {
		try {
		    exportController.exportFile(new File(chooser.getSelectedFile() + ".png"));
		    logPanel.log("Graph exported as png file '" + chooser.getSelectedFile().getName() + ".png'" );
		} catch (IOException ex) {
		    Exceptions.printStackTrace(ex);
		}
	    }
	    else
	    {
		logPanel.log("User cancelled export");
	    }
	});
	
	// Setup menubar imports
	// Import arc data
	menuBar.addImportArcActionListener((ActionEvent e) -> {
	    arcImport();
	});
	
	// Import node data
	menuBar.addImportNodeActionListener((ActionEvent e) -> {
	    nodeImport();
	});
	
	// Import path data
	menuBar.addImportPathActionListener((ActionEvent e) -> {
	    JFileChooser chooser = new JFileChooser();
	    int result = chooser.showSaveDialog(null);
	    
	    if (result == JFileChooser.APPROVE_OPTION) {
		dataPanel.updatePathTable(chooser.getSelectedFile());
		logPanel.log("Opening path data from " + chooser.getSelectedFile().getName());
	    }
	    
	});
	
	// Setup dataPanel listeners
	// Listen for path click
	dataPanel.addPathSelectionListener((ListSelectionEvent e) -> {
	    if(e.getValueIsAdjusting())
	    {
		GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
		String[] rowData = dataPanel.getSelectedPathRow();
		previewPanel.setnodeSel(rowData[1]);
		previewPanel.setarcSel(rowData[2]);
		String[] nodeList = rowData[1].split(",");
		// String[] edgeList = rowData[2].split(",");
		
		for(String curNode: nodeList)
		{
		    // skip Source and Sink colouring (removes alot of sub-deployment colouring due to source and sink neighbouring)
//		    try {
//			String trainId = graphController.getGraphModel().getGraph().getNode(curNode).getAttribute("trainID").toString();
//			if(trainId.equals("SOURCE") || trainId.equals("SINK"))
//			{
//			    continue;
//			}
//		    } catch (Exception x) {
//		    }
		    graphController.getGraphModel().getGraph().getNode(curNode).setColor(Color.red);
		}
		previewController.refreshPreview();
	    }
	});
    }
}