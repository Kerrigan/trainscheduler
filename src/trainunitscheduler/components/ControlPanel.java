package trainunitscheduler.components;

import java.awt.Color;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.NodeIterable;

/**
 *
 * @author Connor
 */
public class ControlPanel extends javax.swing.JPanel {
    
    private String colourNames[] = {"White", "Black", "Dark Gray", "Gray", "Green", "Light Gray",
	"Magenta", "Orange", "Pink", "Red", "Blue", "Yellow"};
    private Color colours[] = {Color.white, Color.black,  Color.darkGray, Color.gray, Color.green, Color.lightGray,
	Color.magenta, Color.orange, Color.pink, Color.red, Color.blue, Color.yellow};
    
     /**
     * Creates new form ControlPanel
     */
    public ControlPanel() {
	initComponents();
	addcolours();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ControlPanelLabel = new javax.swing.JLabel();
        OpenArcButton = new javax.swing.JButton();
        OpenNodeButton = new javax.swing.JButton();
        OpenGraphButton = new javax.swing.JButton();
        ImportDataTitleLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        BackgroundColourLabel = new javax.swing.JLabel();
        BackgroundColourComboBox = new javax.swing.JComboBox();
        DesignTitleLabel = new javax.swing.JLabel();
        NodePartitionLabel = new javax.swing.JLabel();
        LayoutButton = new javax.swing.JButton();
        LayoutComboBox = new javax.swing.JComboBox();
        jSeparator4 = new javax.swing.JSeparator();
        NodePartitionApplyButton = new javax.swing.JButton();
        NodePartitionComboBox = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        ShowLabelsCheckBox = new javax.swing.JCheckBox();
        NodeLabelComboBox = new javax.swing.JComboBox();
        ArcLabelComboBox = new javax.swing.JComboBox();
        NodeLabelsLabel = new javax.swing.JLabel();
        ArcLabelsLabel = new javax.swing.JLabel();
        LabelColourLabel = new javax.swing.JLabel();
        LabelColourComboBox = new javax.swing.JComboBox();
        DesignResetButton = new javax.swing.JButton();
        FilterTitleLabel = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        ByDegreeLabel = new javax.swing.JLabel();
        DegreeOperatorComboBox = new javax.swing.JComboBox();
        DegreeValueTextField = new javax.swing.JTextField();
        ByDegreeFilterButton = new javax.swing.JButton();
        FilterResetButton = new javax.swing.JButton();
        ByStationLabel = new javax.swing.JLabel();
        ByStationComboBox = new javax.swing.JComboBox();
        ByStationApplyButton = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(200, 0));

        ControlPanelLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        ControlPanelLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        org.openide.awt.Mnemonics.setLocalizedText(ControlPanelLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.ControlPanelLabel.text")); // NOI18N

        OpenArcButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(OpenArcButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.OpenArcButton.text")); // NOI18N
        OpenArcButton.setPreferredSize(new java.awt.Dimension(120, 32));

        OpenNodeButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(OpenNodeButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.OpenNodeButton.text")); // NOI18N
        OpenNodeButton.setEnabled(false);
        OpenNodeButton.setPreferredSize(new java.awt.Dimension(312, 32));

        OpenGraphButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(OpenGraphButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.OpenGraphButton.text")); // NOI18N
        OpenGraphButton.setToolTipText(org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.OpenGraphButton.toolTipText")); // NOI18N
        OpenGraphButton.setPreferredSize(new java.awt.Dimension(150, 32));

        ImportDataTitleLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(ImportDataTitleLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.ImportDataTitleLabel.text")); // NOI18N

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jLabel6.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jLabel6, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.jLabel6.text")); // NOI18N
        jLabel6.setToolTipText(org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.jLabel6.toolTipText")); // NOI18N

        BackgroundColourLabel.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(BackgroundColourLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.BackgroundColourLabel.text")); // NOI18N
        BackgroundColourLabel.setToolTipText(org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.BackgroundColourLabel.toolTipText")); // NOI18N

        BackgroundColourComboBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        DesignTitleLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(DesignTitleLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.DesignTitleLabel.text")); // NOI18N

        NodePartitionLabel.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(NodePartitionLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.NodePartitionLabel.text")); // NOI18N
        NodePartitionLabel.setToolTipText(org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.NodePartitionLabel.toolTipText")); // NOI18N

        LayoutButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(LayoutButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.LayoutButton.text")); // NOI18N

        LayoutComboBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        LayoutComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Expand", "Contract", "YifanHu", "Force Atlas", "Fruchterman Reingold", "Random" }));

        NodePartitionApplyButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(NodePartitionApplyButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.NodePartitionApplyButton.text")); // NOI18N

        NodePartitionComboBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        NodePartitionComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Source", "Destination" }));

        jLabel1.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.jLabel1.text")); // NOI18N
        jLabel1.setToolTipText(org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.jLabel1.toolTipText")); // NOI18N

        ShowLabelsCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        ShowLabelsCheckBox.setSelected(true);
        org.openide.awt.Mnemonics.setLocalizedText(ShowLabelsCheckBox, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.ShowLabelsCheckBox.text")); // NOI18N

        NodeLabelComboBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        ArcLabelComboBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        NodeLabelsLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(NodeLabelsLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.NodeLabelsLabel.text")); // NOI18N

        ArcLabelsLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(ArcLabelsLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.ArcLabelsLabel.text")); // NOI18N

        LabelColourLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(LabelColourLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.LabelColourLabel.text")); // NOI18N

        LabelColourComboBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(DesignResetButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.DesignResetButton.text")); // NOI18N
        DesignResetButton.setToolTipText(org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.DesignResetButton.toolTipText")); // NOI18N

        FilterTitleLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(FilterTitleLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.FilterTitleLabel.text")); // NOI18N

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));

        ByDegreeLabel.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(ByDegreeLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.ByDegreeLabel.text")); // NOI18N

        DegreeOperatorComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<", "=", ">" }));

        DegreeValueTextField.setText(org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.DegreeValueTextField.text")); // NOI18N

        ByDegreeFilterButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(ByDegreeFilterButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.ByDegreeFilterButton.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(FilterResetButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.FilterResetButton.text")); // NOI18N

        ByStationLabel.setFont(new java.awt.Font("Dialog", 3, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(ByStationLabel, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.ByStationLabel.text")); // NOI18N

        ByStationApplyButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(ByStationApplyButton, org.openide.util.NbBundle.getMessage(ControlPanel.class, "ControlPanel.ByStationApplyButton.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(DesignTitleLabel)
                        .addGap(30, 30, 30))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(91, 91, 91)
                                .addComponent(FilterTitleLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ByDegreeLabel)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(DegreeOperatorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(DegreeValueTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(FilterResetButton)
                    .addComponent(ByDegreeFilterButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
            .addComponent(jSeparator3)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator6))
                .addGap(6, 6, 6))
            .addComponent(jSeparator2)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jSeparator7)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(BackgroundColourLabel)
                        .addGap(18, 18, 18)
                        .addComponent(BackgroundColourComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LayoutComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(NodePartitionComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(NodePartitionApplyButton)
                                    .addComponent(LayoutButton)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ShowLabelsCheckBox)
                                    .addComponent(NodeLabelsLabel)
                                    .addComponent(ArcLabelsLabel)
                                    .addComponent(LabelColourLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(NodeLabelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ArcLabelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(LabelColourComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel6))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(NodePartitionLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(OpenArcButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(OpenNodeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(OpenGraphButton, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(ImportDataTitleLabel)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(DesignResetButton)
                                .addGap(6, 6, 6))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(ControlPanelLabel)
                                .addGap(133, 133, 133))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ByStationLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(ByStationComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ByStationApplyButton)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(ControlPanelLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ImportDataTitleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OpenArcButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OpenNodeButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OpenGraphButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DesignTitleLabel)
                    .addComponent(DesignResetButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BackgroundColourLabel)
                    .addComponent(BackgroundColourComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(NodePartitionLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NodePartitionApplyButton)
                    .addComponent(NodePartitionComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LayoutComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LayoutButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ShowLabelsCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NodeLabelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NodeLabelsLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ArcLabelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ArcLabelsLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelColourLabel)
                    .addComponent(LabelColourComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(FilterTitleLabel)
                    .addComponent(FilterResetButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ByDegreeLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DegreeOperatorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DegreeValueTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ByDegreeFilterButton))
                .addGap(12, 12, 12)
                .addComponent(ByStationLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ByStationComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ByStationApplyButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    // Set control panel layout
    public ControlPanel(LayoutManager layout) {
	super(layout);
    }
    
    // Method to enable or disable Node import button
    public void nodeButtonControl(boolean enabled)
    {
	OpenNodeButton.setEnabled(enabled);
    }
    
    // Listener setup methods
    public void addNodeActionListener(ActionListener al)
    {
	OpenNodeButton.addActionListener(al);
    }
    public void addArcActionListener(ActionListener al)
    {
	OpenArcButton.addActionListener(al);
    }
    public void addOpenActionListener(ActionListener al)
    {
	OpenGraphButton.addActionListener(al);
    }
    public void addBackComboListener(ActionListener al)
    {
	BackgroundColourComboBox.addActionListener(al);
    }
    public void addApplyLayoutListener(ActionListener al)
    {
	LayoutButton.addActionListener(al);
    }
    public void addApplyNodePartitionListener(ActionListener al)
    {
	NodePartitionApplyButton.addActionListener(al);
    }
    public void addShowLabelListener(ActionListener al)
    {
	ShowLabelsCheckBox.addActionListener(al);
    }
    public void addNodeLabelListener(ActionListener al)
    {
	NodeLabelComboBox.addActionListener(al);
    }
    public void addArcLabelListener(ActionListener al)
    {
	ArcLabelComboBox.addActionListener(al);
    }
    public void addLabelColourListener(ActionListener al)
    {
	LabelColourComboBox.addActionListener(al);
    }
    public void addDesignResetButtonListener(ActionListener al)
    {
	DesignResetButton.addActionListener(al);
    }
    public void addFilterResetButtonListener(ActionListener al)
    {
	FilterResetButton.addActionListener(al);
    }
    public void addApplyByDegreeFilterListener(ActionListener al)
    {
	ByDegreeFilterButton.addActionListener(al);
    }
    public void addApplyByStationFilterListener(ActionListener al)
    {
	ByStationApplyButton.addActionListener(al);
    }
    
    
    // Retrieves the selected background colour, returns colourPicker consists of the colour object and colour name string
    public ColourPicker getBackColour()
    {
	ColourPicker colourPicker = new ColourPicker(colours[BackgroundColourComboBox.getSelectedIndex()], colourNames[BackgroundColourComboBox.getSelectedIndex()]);
	return colourPicker;
    }
    
    // Retrieves selected partition method
    public String getSelNodePartition()
    {
	return NodePartitionComboBox.getSelectedItem().toString();
    }
    
    // Retrieves selected layout from LayoutComboBox
    public String getSelLayout()
    {
	return LayoutComboBox.getSelectedItem().toString();
    }
    
    // Returns boolean show label
    public boolean getShowLabel()
    {
	return ShowLabelsCheckBox.isSelected();
    }
    
    // Sets boolean show label
    public void setShowLabel(boolean showLabel)
    {
	ShowLabelsCheckBox.setSelected(showLabel);
    }
    
    // Sets the column names in combo box
    public void setNodeColumns(String[] columns)
    {
	NodeLabelComboBox.removeAllItems();
	NodeLabelComboBox.addItem("None");
	
	for(String col : columns)
	{
	    // Skip label and interval
	    if(col.equals("Label") || col.equals("Interval"))
	    {
		continue;
	    }
	    NodeLabelComboBox.addItem(col);
	}
    }
    
    // Sets the column names in combo box
    public void setArcColumns(String[] columns)
    {
	ArcLabelComboBox.removeAllItems();
	ArcLabelComboBox.addItem("None");
	
	for(String col : columns)
	{
	    // Skip label, interval and weight
	    if(col.equals("Label") || col.equals("Weight") || col.equals("Interval"))
	    {
		continue;
	    }
	    ArcLabelComboBox.addItem(col);
	}
    }
    
    // Get the selected Node label column
    public String getNodeLabelColumn()
    {
	return NodeLabelComboBox.getSelectedItem().toString();
    }
    
    // Get the selected Arc label column
    public String getArcLabelColumn()
    {
	return ArcLabelComboBox.getSelectedItem().toString();
    }
    
    // Get the selected label colour
    public ColourPicker getLabelColour()
    {
	ColourPicker colourPicker = new ColourPicker(colours[LabelColourComboBox.getSelectedIndex()], colourNames[LabelColourComboBox.getSelectedIndex()]);
	return colourPicker;
    }
    
    // Get degree filter options
    public String[] getDegreeFilter()
    {
	String[] filterData = new String[2];
	filterData[0] = DegreeOperatorComboBox.getSelectedItem().toString();
	filterData[1] = DegreeValueTextField.getText();
	return filterData;
    }
    
    // Sets the station names in combo box
    public void setNodeStations(Graph graph)
    {
	ByStationComboBox.removeAllItems();
	
	NodeIterable nodes = graph.getNodes();
	
	List stationList = new ArrayList();
	
	for(Node node: nodes)
	{
	    String sourceStation = node.getAttribute("Source").toString();
	    String destinationStation = node.getAttribute("Destination").toString();
	    
	    if(!stationList.contains(sourceStation))
	    {
		stationList.add(sourceStation);
	    }
	    
	    if(!stationList.contains(destinationStation))
	    {
		stationList.add(destinationStation);
	    }
	}
	
	for(Object station: stationList)
	{
	    ByStationComboBox.addItem(station);
	}
    }
    
    // Returns the selected station
    public String getStationFilter()
    {
	return ByStationComboBox.getSelectedItem().toString();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox ArcLabelComboBox;
    private javax.swing.JLabel ArcLabelsLabel;
    private javax.swing.JComboBox BackgroundColourComboBox;
    private javax.swing.JLabel BackgroundColourLabel;
    private javax.swing.JButton ByDegreeFilterButton;
    private javax.swing.JLabel ByDegreeLabel;
    private javax.swing.JButton ByStationApplyButton;
    private javax.swing.JComboBox ByStationComboBox;
    private javax.swing.JLabel ByStationLabel;
    private javax.swing.JLabel ControlPanelLabel;
    private javax.swing.JComboBox DegreeOperatorComboBox;
    private javax.swing.JTextField DegreeValueTextField;
    private javax.swing.JButton DesignResetButton;
    private javax.swing.JLabel DesignTitleLabel;
    private javax.swing.JButton FilterResetButton;
    private javax.swing.JLabel FilterTitleLabel;
    private javax.swing.JLabel ImportDataTitleLabel;
    private javax.swing.JComboBox LabelColourComboBox;
    private javax.swing.JLabel LabelColourLabel;
    private javax.swing.JButton LayoutButton;
    private javax.swing.JComboBox LayoutComboBox;
    private javax.swing.JComboBox NodeLabelComboBox;
    private javax.swing.JLabel NodeLabelsLabel;
    private javax.swing.JButton NodePartitionApplyButton;
    private javax.swing.JComboBox NodePartitionComboBox;
    private javax.swing.JLabel NodePartitionLabel;
    private javax.swing.JButton OpenArcButton;
    private javax.swing.JButton OpenGraphButton;
    private javax.swing.JButton OpenNodeButton;
    private javax.swing.JCheckBox ShowLabelsCheckBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    // End of variables declaration//GEN-END:variables
    
    // Adds colours to combo boxes
    private void addcolours() {
	for (String col : colourNames)
	{
	    BackgroundColourComboBox.addItem(col);
	    LabelColourComboBox.addItem(col);
	}
	BackgroundColourComboBox.setSelectedIndex(0);
    }
    
    // Class to return color and color name
    public class ColourPicker
    {
	private Color colour;
	private String colour_name;
	
	public ColourPicker(Color colour, String colour_name) {
	    this.colour = colour;
	    this.colour_name = colour_name;
	}
	
	public Color getColour() {
	    return colour;
	}
	
	public String getColourName() {
	    return colour_name;
	}
    }
}
