package trainunitscheduler;

import trainunitscheduler.components.MainFrame;

/**
 *
 * @author Connor
 */
public class RunScheduleVis {
        public static void main(String[] args) {
	// Initialises and starts schedule visualisation application
        MainFrame mainFrame = new MainFrame();
        mainFrame.script();
	mainFrame.setTitle("Interactive Train Unit Schedule Visualisation");
    }
}
