package trainunitscheduler.functions;

import org.gephi.graph.api.GraphModel;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.plugin.force.yifanHu.YifanHuLayout;
import org.gephi.layout.plugin.forceAtlas.ForceAtlasLayout;
import org.gephi.layout.plugin.fruchterman.FruchtermanReingold;
import org.gephi.layout.plugin.random.RandomLayout;
import org.gephi.layout.plugin.scale.ContractLayout;
import org.gephi.layout.plugin.scale.ExpandLayout;
import org.gephi.preview.api.PreviewController;

/**
 *
 * @author Connor
 */
public class ApplyLayout {
    public static void applyLayout(String layout, PreviewController previewController, GraphModel graphModel)
    {
	if(layout.equals("Expand"))
	{
	    ExpandLayout expandLayout = new ExpandLayout(null, 1.2);
	    expandLayout.setGraphModel(graphModel);
	    
	    expandLayout.initAlgo();
	    for (int i = 0; i < 100 && expandLayout.canAlgo(); i++) {
		expandLayout.goAlgo();
	    }
	    expandLayout.endAlgo();
	}
	else if(layout.equals("Contract"))
	{
	    ContractLayout contractLayout = new ContractLayout(null, new Double(0.8));
	    contractLayout.setGraphModel(graphModel);
	    
	    
	    contractLayout.initAlgo();
	    for (int i = 0; i < 100 && contractLayout.canAlgo(); i++) {
		contractLayout.goAlgo();
	    }
	    contractLayout.endAlgo();
	}
	else if(layout.equals("YifanHu"))
	{
	    // Applies the Yifan Hu layout
	    YifanHuLayout yifanHuLayout = new YifanHuLayout(null, new StepDisplacement(1f));
	    yifanHuLayout.setGraphModel(graphModel);
	    
	    yifanHuLayout.initAlgo();
	    yifanHuLayout.resetPropertiesValues();
	    yifanHuLayout.setOptimalDistance(100f);
	    for (int i = 0; i < 100 && yifanHuLayout.canAlgo(); i++) {
		yifanHuLayout.goAlgo();
	    }
	    yifanHuLayout.endAlgo();
	}
	else if(layout.equals("Force Atlas"))
	{
	    // Applies the Force Atlas layout
	    ForceAtlasLayout forceAtlasLayout = new ForceAtlasLayout(null);
	    forceAtlasLayout.setGraphModel(graphModel);
	    
	    forceAtlasLayout.initAlgo();
	    
	    forceAtlasLayout.resetPropertiesValues();
	    forceAtlasLayout.setAttractionStrength(new Double(1));
	    forceAtlasLayout.setRepulsionStrength(new Double(400));
	    for (int i = 0; i < 1000 && forceAtlasLayout.canAlgo(); i++) {
		forceAtlasLayout.goAlgo();
	    }
	    forceAtlasLayout.endAlgo();
	}
	else if(layout.equals("Fruchterman Reingold"))
	{
	    // Applies the Fruchterman-Reingold layout
	    FruchtermanReingold fruchtermanReingold = new FruchtermanReingold(null);
	    fruchtermanReingold.setGraphModel(graphModel);
	    
	    fruchtermanReingold.initAlgo();
	    
	    fruchtermanReingold.resetPropertiesValues();
	    fruchtermanReingold.setArea(10000f);
	    for(int i = 0; i < 500 && fruchtermanReingold.canAlgo(); i++)
	    {
		fruchtermanReingold.goAlgo();
	    }
	    fruchtermanReingold.endAlgo();
	}
	else if(layout.equals("Random"))
	{
	    // Applies a Random layout
	    RandomLayout randomLayout = new RandomLayout(null, new Double(2000));
	    randomLayout.setGraphModel(graphModel);
	    
	    randomLayout.initAlgo();
	    
	    randomLayout.resetPropertiesValues();
	    for(int i = 0; i < 100 && randomLayout.canAlgo(); i++)
	    {
		randomLayout.goAlgo();
	    }
	    randomLayout.endAlgo();
	}
//	    System.err.println(layout + " Layout applied");
	
	previewController.refreshPreview();
    }
}
