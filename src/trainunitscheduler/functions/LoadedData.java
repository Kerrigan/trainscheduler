package trainunitscheduler.functions;

/**
 *
 * @author Connor
 */
// Class to keep track of data currently loaded
public class LoadedData {
    
    
    private boolean fileloaded;
    private boolean nodesloaded;
    private boolean arcsloaded;
    
    public LoadedData() {
	this.fileloaded = false;
	this.nodesloaded = false;
	this.arcsloaded = false;
    }
    
    public boolean isFileloaded() {
	return fileloaded;
    }
    
    public boolean isNodesloaded() {
	return nodesloaded;
    }
    
    public boolean isArcsloaded() {
	return arcsloaded;
    }
    
    public void setFileloaded(boolean fileloaded) {
	this.fileloaded = fileloaded;
    }
    
    public void setNodesloaded(boolean nodesloaded) {
	this.nodesloaded = nodesloaded;
    }
    
    public void setArcsloaded(boolean arcsloaded) {
	this.arcsloaded = arcsloaded;
    }
    
    
}

