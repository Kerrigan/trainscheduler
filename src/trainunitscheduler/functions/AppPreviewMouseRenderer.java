package trainunitscheduler.functions;

import org.gephi.preview.api.CanvasSize;
import org.gephi.preview.api.Item;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperties;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.api.RenderTarget;
import org.gephi.preview.spi.ItemBuilder;
import org.gephi.preview.spi.MouseResponsiveRenderer;
import org.gephi.preview.spi.PreviewMouseListener;
import org.gephi.preview.spi.Renderer;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Connor
 */
@ServiceProvider(service = Renderer.class)
public class AppPreviewMouseRenderer implements MouseResponsiveRenderer, Renderer{
    
    @Override
    public boolean needsPreviewMouseListener(PreviewMouseListener previewMouseListener) {
	return previewMouseListener instanceof AppPreviewMouseListener;
    }
    
    @Override
    public String getDisplayName() {
	return "Renderer";
    }
    
    @Override
    public void preProcess(PreviewModel previewModel) {
    }
    
    @Override
    public void render(Item item, RenderTarget target, PreviewProperties properties) {
    }
    
    @Override
    public PreviewProperty[] getProperties() {
	return new PreviewProperty[0];
    }
    
    @Override
    public boolean isRendererForitem(Item item, PreviewProperties properties) {
	return false;
    }
    
    @Override
    public boolean needsItemBuilder(ItemBuilder itemBuilder, PreviewProperties properties) {
	return false;
    }
    
    @Override
    public CanvasSize getCanvasSize(Item item, PreviewProperties pp) {
	return new CanvasSize();
    }
    
}
