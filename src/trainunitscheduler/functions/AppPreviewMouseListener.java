package trainunitscheduler.functions;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.PreviewMouseEvent;
import org.gephi.preview.api.PreviewProperties;
import org.gephi.preview.spi.PreviewMouseListener;
import org.gephi.project.api.Workspace;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Connor
 */
@ServiceProvider(service = PreviewMouseListener.class)
public class AppPreviewMouseListener implements PreviewMouseListener {
    
    // Retrieves the clicked node data and saves it to csv file
    @Override
    public void mouseClicked(PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {
	Graph graph = Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace).getGraph();
	for (Node node : graph.getNodes()) {
	    if (clickInNode(node, event)) {
		try {
		    node.setAlpha(0);
//		    properties.putValue("display-label.node.id", node.getId());
//		    System.err.println("Node " + node.getId() + " clicked!");
		    FileWriter fileWriter;
		    fileWriter = new FileWriter("clickednodeFile.csv");
		    BufferedWriter bufferedWriter;
		    bufferedWriter = new BufferedWriter(fileWriter);
		    
		    try {
			bufferedWriter.write("Id,"+node.getId().toString() + "\n");
			bufferedWriter.write("Label,"+node.getLabel() + "\n");
			bufferedWriter.write("trainID,"+node.getAttribute("trainID") + "\n");
			bufferedWriter.write("Source,"+node.getAttribute("Source") + "\n");
			bufferedWriter.write("Departure-Time,"+node.getAttribute("Departure-Time") + "\n");
			bufferedWriter.write("Destination,"+node.getAttribute("Destination") + "\n");
			bufferedWriter.write("Arrival-Time,"+node.getAttribute("Arrival-Time") + "\n");
		    } catch (IllegalArgumentException e) {
		    }
		    
		    graph.getNeighbors(node, 0);
		    bufferedWriter.close();
		    fileWriter.close();
		    
		    //renderer is executed and the graph repainted
		    event.setConsumed(true);
		    return;
		} catch (IOException ex) {
		    Exceptions.printStackTrace(ex);
		}
	    }
	}
	event.setConsumed(true);//So the renderer is executed and the graph repainted
    }
    
    @Override
    public void mousePressed(PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {
//	System.err.println("Mouse Pressed");
    }
    
    @Override
    public void mouseDragged(PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {
//	System.err.println("Mouse Dragged");
    }
    
    @Override
    public void mouseReleased(PreviewMouseEvent event, PreviewProperties properties, Workspace workspace) {
//	System.err.println("Mouse Released");
    }
    
    // Tests whether the mouse clicked on a node or not by using x, y coordinates and node radius
    private boolean clickInNode(Node node, PreviewMouseEvent event) {
	
	float xdiff = node.x() - event.x;
	//y axis is inverse for node coordinates
	float ydiff = -node.y() - event.y;
	float radius = node.size();
	
	return xdiff * xdiff + ydiff * ydiff < radius * radius;
    }
}
