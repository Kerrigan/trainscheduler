package trainunitscheduler.functions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.nio.file.Files;


/**
 *
 * @author Connor
 */
public class FileParser {
    
    public static File parseArc(File file)
    {
	try {
	    // Parse arc xml file, extract arc and path data
	    if(file.getName().contains(".xml"))
	    {
		// Load and read file data
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName("optArcSol");
		
		File arcfile = new File("arc.csv");
		
		FileWriter fileWriter;
		fileWriter = new FileWriter(arcfile);
		BufferedWriter bufferedWriter;
		bufferedWriter = new BufferedWriter(fileWriter);
		bufferedWriter.append("Id,fromTrain,toTrain,Source,Target,arcKind,Value\n");
		
		
		// For each arc element in file
		for (int temp = 0; temp < nList.getLength(); temp++)
		{
		    Node nNode = nList.item(temp);
		    
		    if (nNode.getNodeType() == Node.ELEMENT_NODE)
		    {
			Element eElement = (Element) nNode;
			
			NamedNodeMap attributes = eElement.getAttributes();
			
			// Add all arc data to file
			bufferedWriter.append("" + attributes.getNamedItem("arcIndx").getNodeValue() + "," + attributes.getNamedItem("fromTrain").getNodeValue() + "," + attributes.getNamedItem("toTrain").getNodeValue() +  "," + attributes.getNamedItem("fromTrainIdx").getNodeValue() + "," + attributes.getNamedItem("toTrainIdx").getNodeValue() + "," + attributes.getNamedItem("arcKind").getNodeValue() + "," + attributes.getNamedItem("Value").getNodeValue() + "\n");
		    }
		}
		bufferedWriter.close();
		fileWriter.close();
		
		
		// Save path data to path.csv file
		nList = doc.getElementsByTagName("optPathSol");
		
		File pathfile = new File("path.csv");
		
		fileWriter = new FileWriter(pathfile);
		bufferedWriter = new BufferedWriter(fileWriter);
		
		bufferedWriter.append("PathNodeList,PathArcList,NumberUnits,Cost\n");
		
		// For each path element in file
		for (int temp = 0; temp < nList.getLength(); temp++)
		{
		    Node nNode = nList.item(temp);
		    
		    if (nNode.getNodeType() == Node.ELEMENT_NODE)
		    {
			Element eElement = (Element) nNode;
			
			NamedNodeMap attributes = eElement.getAttributes();
			
			// Add all arc data to file
			bufferedWriter.append("" + attributes.getNamedItem("PathNodeList").getNodeValue().replace(",", ";") + "," + attributes.getNamedItem("PathArcList").getNodeValue().replace(",", ";") +  "," + attributes.getNamedItem("NumberUnits").getNodeValue() + "," +  attributes.getNamedItem("Cost").getNodeValue() + "\n");
		    }
		}
		
		bufferedWriter.close();
		fileWriter.close();
		
		return arcfile;
	    }
	    else if(file.getName().contains(".csv"))
	    {
		return file;
	    }
	    
	}catch (IOException | ParserConfigurationException | SAXException ex) {
	    Exceptions.printStackTrace(ex);
	}
	return null;
    }
    
    
    public static File parseNode(File file)
    {
	if(file.getName().contains(".dat"))
	{
	    File inputFile = file;
	    ArrayList<String> data = new ArrayList<String>();
	    
	    try
	    {
		String[] lines = Files.readAllLines(inputFile.toPath()).toArray(new String[0]);
		
		for (String line : lines)
		{
		    data.add(line);
		}
	    } catch (IOException e)
	    {
		e.printStackTrace();
	    }
	    
	    File outfile = new File("node.csv");
	    
//	ArrayList<String> values = data;
	    FileWriter fileWriter;
	    try
	    {
		int count = 1;
		fileWriter = new FileWriter(outfile);
		
		// create CSVWriter object filewriter object as parameter
		BufferedWriter bufferedWriter;
		bufferedWriter = new BufferedWriter(fileWriter);
		bufferedWriter.append("Id,trainID,Source,Departure-Time,Destination,Arrival-Time\n");
//	    bufferedWriter.append("Id,trainID,Day,Man,Source,Departure-Time,Destination,Arrival-Time,num1,num2,num3,num4\n");
		
//	    bufferedWriter.append("" + String.valueOf(count) + "," + "SOURCE" + "," + null + "," + null + "," + null + "," + "00.00" + "," + null + "," + "00.00" + "," + null + "," + null + "," + null + "," + null + "\n");
		bufferedWriter.append("" + String.valueOf(count) + "," + "SOURCE" + "," + null + "," + "00.00" + "," + null + "," + "00.00" + "\n");
		
		count++;
		
		Pattern pattern = Pattern.compile("'([^']+)' '([^']+)' '([^']+)' '([^']+)' 'P' \\[(-?\\d\\d -?\\d\\d)\\] \\* '([^']+)' 'P' \\[(-?\\d\\d  -?\\d\\d)] \\* 0 \\['387' ] \\* \\* \\* 0 0 1 1 [12] \\[(-?\\d ?-?\\d?) \\] \\[(-?\\d ?-?\\d?) \\] \\[(-?\\d ?-?\\d?) \\] (\\d{3}) \\*");
		
		Matcher match;
		
		for (String location : data)
		{
		    if("]".equals(location))
		    {
			break;
		    }
		    
		    match = pattern.matcher(location);
		    
		    if(match.find())
		    {
			double depart = Double.valueOf(match.group(5).replace(" ", "."));
			
			double arrive = Double.valueOf(match.group(7).replace("  ", "."));
			
//		    bufferedWriter.append("" + String.valueOf(count) + "," + match.group(1) + "," + match.group(2) + "," + match.group(3) + "," + match.group(4) + "," + String.valueOf(depart) + "," + match.group(6) + "," + String.valueOf(arrive) + "," + match.group(8) + "," + match.group(9) + "," + match.group(10)+ "," + match.group(11) + "\n");
			bufferedWriter.append("" + String.valueOf(count) + "," + match.group(1) + "," + match.group(4) + "," + String.valueOf(depart) + "," + match.group(6) + "," + String.valueOf(arrive) + "\n");
			
			count++;
		    }
		}
		
//	    bufferedWriter.append("" + String.valueOf(count) + "," + "SINK" + "," + null + "," + null + "," + null + "," + "44.00" + "," + null + "," + "48.00" + "," + null + "," + null + "," + null + "," + null + "");
		bufferedWriter.append("" + String.valueOf(count) + "," + "SINK" + "," + null + "," + "44.00" + "," + null + "," + "48.00" + "\n");
		bufferedWriter.close();
		fileWriter.close();
		
		return outfile;
		
		
	    } catch (IOException e)
	    {
	    }
	}
	else if(file.getName().contains(".csv"))
	{
	    return file;
	}
	return null;
    }
}
